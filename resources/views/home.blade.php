@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel-heading">Dashboard</div>
			<div class="panel-body">
				<form method="POST" action="{{url('add')}}">
					<input type="text" class="form-control" placeholder="your article" name="title"><br>
					<textarea rows="10" name="body" class="form-control" placeholder="main article"></textarea>
					<button type="submit" class="btn btn-primary">Add Article</button>

					<input type="hidden" name="_token" value="{{csrf_token()}}">
				</form>
			</div>
		</div>
	</div>
</div>
@endsection